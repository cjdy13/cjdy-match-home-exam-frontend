"use client";
import { Button } from "@nextui-org/button";
import { Table, TableHeader, TableColumn, TableBody, TableRow, TableCell } from "@nextui-org/table";
import {Card, CardBody, CardFooter} from "@nextui-org/card";
import { Divider } from "@nextui-org/divider";

export default function Home() {
  const data = [];
  var i;
  for(i=0; i < 6; i++) {
    data.push({property_type: 'Amaia Laguna', model:'Amaia Lands', location: 'Laguna', area: '50sqm', price: '2.2m', availability: 'Ready for Occupancy'});
  }
  return (
    <main className="flex flex-col items-center justify-between" style={{backgroundColor:'#1d3461'}}>
      <div style={{height: '40vh', width: "100% !important", backgroundImage:`url("https://www.amaialand.com/static/H&L%201-b524a090d7f6f34c2722809efe75d3d9.jpg")`, backgroundSize: "cover", backgroundRepeat: "no-repeat", backgroundPositionY: '-290px'}}>
        <div className="flex flex-col inline-block align-middle justify-center" style={{fontSize: '60px'}}>
          <span style={{color:'#1d3461', zIndex:'1000', fontFamily: 'Inter'}}>Amaia Scapes Laguna</span>
          <p style={{color:'#007e66'}}>PROPERTY LIST</p>
        </div>
      </div>
      <div className="container">
        {data.map((e) => {
          return <Card radius="lg" className="mt-10">
            <CardBody className="px-9 py-5">
              <Table style={{textAlign: 'center', overflowX: 'auto'}} removeWrapper>
                <TableHeader>
                  <TableColumn style={{textAlign: 'center', fontWeight: 'bold', fontSize: '14px', backgroundColor: "transparent !important"}}>Property Type</TableColumn>
                  <TableColumn style={{textAlign: 'center', fontWeight: 'bold', fontSize: '14px', backgroundColor: "transparent !important"}}>Model</TableColumn>
                  <TableColumn style={{textAlign: 'center', fontWeight: 'bold', fontSize: '14px', backgroundColor: "transparent !important"}}>Location</TableColumn>
                  <TableColumn style={{textAlign: 'center', fontWeight: 'bold', fontSize: '14px', backgroundColor: "transparent !important"}}>Area</TableColumn>
                  <TableColumn style={{textAlign: 'center', fontWeight: 'bold', fontSize: '14px', backgroundColor: "transparent !important"}}>Price</TableColumn>
                  <TableColumn style={{textAlign: 'center', fontWeight: 'bold', fontSize: '14px', backgroundColor: "transparent !important"}}>Availability</TableColumn>
                </TableHeader>
                <TableBody>
                  <TableRow key="1">
                    <TableCell>{e.property_type}</TableCell>
                    <TableCell>{e.model}</TableCell>
                    <TableCell>{e.location}</TableCell>
                    <TableCell>{e.area}</TableCell>
                    <TableCell>{e.price}</TableCell>
                    <TableCell>{e.availability}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </CardBody>
            <Divider/>
            <CardFooter className="text-small justify-between px-14" style={{backgroundColor: '#007e66'}}>
              <Button className="bg-transparent" style={{color:'white'}}> View Property Map</Button>
              <Button style={{backgroundColor:'#ffb20c'}} radius="full"><b>View Property Full Details</b></Button>
            </CardFooter>
          </Card>
        })}
      </div>
    </main>
  );
}
